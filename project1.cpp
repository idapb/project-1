#include <iostream>
#include <cmath>
#include <fstream>
#include <iomanip>
#include <armadillo>
#include <time.h>
using namespace std;
using namespace arma;

double *trisolve(int n, double *a, double *y, double *x){
//Solving Ax=y
//Forward substitution
int i;
double y_new[n+1];
y_new[1] = y[1];
for(i=2; i < n+1; i++){
a[i] -= 1/a[i-1];
y_new[i] = y[i] + y_new[i-1]/a[i-1];
}
//Backward substitution
x[0] = x[n+1] = 0.0;
for(i=n+1; i > 1; i--){
x[i-1] = (y_new[i-1] + x[i])/a[i-1];
}
return x;
}

void lusolve(int n, double *w_temp){
//LU decomposition
mat A = zeros(n,n), L, U;
vec a1(n);
vec a2(n-1);
A.diag() = a1.fill(2);
A.diag(1) = a2.fill(-1);
A.diag(-1) = a2;
lu(L, U, A);
int i;
vec w(n);
for(i=0; i < n; i++){
w[i] = w_temp[i+1];
}
vec y = solve(L,w);
vec x = solve(U,y);
}

int main(int argc, char* argv[]){
//Declaration of variables
int i;
int n = atof(argv[1]);
double h = 1.0/(n+1.0);
double b[n+2], b_tilde[n+2], f[n+2], x[n+2], u[n+2], e[n+2], v_temp[n+2];
for(i=0; i < n+2; i++){
x[i] = i*h;
f[i] = 100*exp(-10*x[i]);
b[i] = 2;
b_tilde[i] = f[i]*h*h;
u[i] = 1 - (1 - exp(-10))*x[i] - exp(-10*x[i]);
}

//Calling tridiagonal solver and taking the time
clock_t start, finish;
start = clock();
double *v = trisolve(n, b, b_tilde, v_temp);
finish = clock();
cout <<  "Time usage for tridiagonal solver: " << double (finish - start)/CLOCKS_PER_SEC << "s" << endl;

//Saving values to file for plotting
ofstream myfile;
myfile.open("values for plotting.txt");
for(i=0; i < n+2; i++){
myfile << setprecision(15) << v[i] << " " << u[i] << " " << x[i] << endl;
}
myfile.close();

//Calculating the error for tridiagonal solver
double max = 0.0;
for(i=1; i < n+2; i++){
e[i] = log10(abs((v[i]-u[i])/u[i]));
if(abs(e[i]) > abs(max)){
max = e[i];}
}
cout << "Max value of the relative error: " << max << endl;
cout << "log10(h): " << log10(h) << endl;

//Calling lu-solver and taking the time
start = clock();
lusolve(n, b_tilde);
finish = clock();
cout << "Time usage for LU decomposition: " << double (finish - start)/CLOCKS_PER_SEC << "s" << endl;
}
