\documentclass[11pt,a4wide]{article}
\usepackage{verbatim}
\usepackage{listings}
\usepackage{graphicx}
\usepackage{a4wide}
\usepackage{color}
\usepackage{amsmath}
\usepackage{amssymb}
%\usepackage[dvips]{epsfig}
\usepackage[T1]{fontenc}
\usepackage{cite} % [2,3,4] --> [2--4]
\usepackage{shadow}
\usepackage{hyperref}

\setcounter{tocdepth}{2}

\lstset{language=c++}
\lstset{alsolanguage=[90]Fortran}
\lstset{basicstyle=\small}
\lstset{backgroundcolor=\color{white}}
\lstset{frame=single}
\lstset{stringstyle=\ttfamily}
\lstset{keywordstyle=\color{red}\bfseries}
\lstset{commentstyle=\itshape\color{blue}}
\lstset{showspaces=false}
\lstset{showstringspaces=false}
\lstset{showtabs=false}
\lstset{breaklines}
\begin{document}

\title{FYS - 4150}
\author{Ida Pauline Buran}
\maketitle

\section*{Project 1}

In this project we are going to solve the one-dimensional Poisson equation
with Dirichlet boundary conditions by rewriting it as a set of linear equations. We are going to use different numerical methods such as Gaussian elimination and LU decomposition for solving these equations and compare the number of floating point operations and the time usage for the different methods.

The source code of the program can be found at https://bitbucket.org/idapb/project-1/

\subsection*{(a)}
The equation we are going to solve is
\[
-u''(x) = f(x), \hspace{0.5cm} x\in(0,1), \hspace{0.5cm} u(0) = u(1) = 0.
\]
with $f(x) = 100e^{-10x}$. It has a closed-form solution given by $u(x) = 1-(1-e^{-10})x-e^{-10x}$ which we will use to compare with our numerical solution.

We can make a discretized approximation, $v_i$, to $u$ whith grid points $x_i=ih$, from $x_0=0$ to $x_{n+1}=1$. The step size is then $h=1/(n+1)$ and the boundary conditions for $v_i$ are $v_0 = v_{n+1} = 0$. The approximation to the second derivative of $u$ is
\[
   -\frac{v_{i+1}+v_{i-1}-2v_i}{h^2} = f_i  \hspace{0.5cm} \mathrm{for} \hspace{0.1cm} i=1,\dots, n,
\]
where $f_i=f(x_i)$.

This can be rewritten as a linear set of equations of the form
\[
   {\bf A}{\bf v} = \tilde{{\bf b}},
\]
where ${\bf A}$ is an $n\times n$  tridiagonal matrix which we rewrite as 
\begin{equation}
    {\bf A} = \left(\begin{array}{cccccc}
                           2& -1& 0 &\dots   & \dots &0 \\
                           -1 & 2 & -1 &0 &\dots &\dots \\
                           0&-1 &2 & -1 & 0 & \dots \\
                           & \dots   & \dots &\dots   &\dots & \dots \\
                           0&\dots   &  &-1 &2& -1 \\
                           0&\dots    &  & 0  &-1 & 2 \\
                      \end{array} \right)
\end{equation}
and $\tilde{b}_i=h^2f_i$.

We can show that this is correct by writing
\begin{equation}
	\left(\begin{array}{cccccc}
                           2& -1& 0 &\dots   & \dots &0 \\
                           -1 & 2 & -1 &0 &\dots &\dots \\
                           0&-1 &2 & -1 & 0 & \dots \\
                           & \dots   & \dots &\dots   &\dots & \dots \\
                           0&\dots   &  &-1 &2& -1 \\
                           0&\dots    &  & 0  &-1 & 2 \\
                      \end{array} \right)
	\left(\begin{array}{cccccc}
                           v_1 \\
                           v_2  \\
                           v_3 \\
                           \dots \\
                           v_{n-1} \\
                           v_n\\
                      \end{array} \right)
=
	\left(\begin{array}{cccccc}
                           \tilde{b}_1\\
                           \tilde{b}_2\\
                           \tilde{b}_3\\
                           \dots \\
                           \tilde{b}_{n-1}\\
                           \tilde{b}_n\\
                      \end{array} \right)
\end{equation}
\begin{equation}
	\Rightarrow\left(\begin{array}{cccccc}
                           2v_1 - v_2 \\
                           -v_1 + 2v_2 - v_3  \\
                           -v_2 + 2v_3 -v_4 \\
                           \dots \\
                           -v_{n-2} + 2v_{n-1} - v_n \\
                           -v_{n-1} + 2v_n\\
                      \end{array} \right)
=
	\left(\begin{array}{cccccc}
                           \tilde{b}_1\\
                           \tilde{b}_2\\
                           \tilde{b}_3\\
                           \dots \\
                           \tilde{b}_{n-1}\\
                           \tilde{b}_n\\
                      \end{array} \right)
\end{equation}
We see that these equations can be written
\begin{equation}
-v_{n-1} + 2v_n - v_{n+1} = \tilde{b}_n = h^2f_i
\end{equation}
which we can rewrite as 
\[
   -\frac{v_{i+1}+v_{i-1}-2v_i}{h^2} = f_i
\]
which is the expression we started with.\\

\subsection*{(b)}
We can write matrix ${\bf A}$ in terms of vectors $a,b$ and $c$ like this:
\begin{equation}
    {\bf A} = \left(\begin{array}{cccccc}
                           b_1& c_1 & 0 &\dots   & \dots &\dots \\
                           a_2 & b_2 & c_2 &\dots &\dots &\dots \\
                           & a_3 & b_3 & c_3 & \dots & \dots \\
                           & \dots   & \dots &\dots   &\dots & \dots \\
                           &   &  &a_{n-2}  &b_{n-1}& c_{n-1} \\
                           &    &  &   &a_n & b_n \\
                      \end{array} \right)\left(\begin{array}{c}
                           v_1\\
                           v_2\\
                           \dots \\
                          \dots  \\
                          \dots \\
                           v_n\\
                      \end{array} \right)
  =\left(\begin{array}{c}
                           \tilde{b}_1\\
                           \tilde{b}_2\\
                           \dots \\
                           \dots \\
                          \dots \\
                           \tilde{b}_n\\
                      \end{array} \right).
\end{equation}

This system can be written as
\begin{equation}
  a_iv_{i-1}+b_iv_i+c_iv_{i+1} = \tilde{b}_i
\end{equation} 
The algorithm for solving this set of linear equations requires a decomposition and forward substitution and a backward substitution. Here is an algorithm for solving the equations which is given in the lecture notes.
\begin{lstlisting}
btemp = b[1];
u[1] = f[1]/btemp;
for(i=2 ; i <= n ; i++) {
   temp[i] = c[i-1]/btemp;
   btemp = b[i]-a[i]*temp[i];
   u[i] = (f[i] - a[i]*u[i-1])/btemp;
}
for(i=n-1 ; i >= 1 ; i--) {
   u[i] -= temp[i+1]*u[i+1];
}
\end{lstlisting}
It requires 8n floating point operations.

For the linear equations we are going to solve we can make the code a little simpler. This is the algorithm we have used:
\begin{lstlisting}
//Solving Ax=y
//Forward substitution
for(i=2; i < n+1; i++){
   a[i] -= 1/a[i-1];
   y_new[i] = y[i] + y_new[i-1]/a[i-1];
}
//Backward substitution
for(i=n+1; i > 1; i--){
   x[i-1] = (y_new[i-1] + x[i])/a[i-1];
}
\end{lstlisting}
This algorithm uses $6n$ floating point operations. If we compare this with standard Gaussian elimination and LU-decomposition which both uses $O(n^3) + O(n^2)$ floating point operations, we see that this algorithm is a lot more efficient.

We have run the program for $n=10, 100$ and $1000$ grid points and plotted the results both for the numerical solution and for the closed-form solution. The plots can be seen in figure \ref{fig:10}, \ref{fig:100} and \ref{fig:1000}.
\begin{figure}[h]
  \centering
    \includegraphics[width=0.7\textwidth]{project1_plot_n=10.pdf}
  \caption{Comparing solutions for $n = 10$ grid points.\label{fig:10}}
\end{figure}
\begin{figure}[h]
  \centering
    \includegraphics[width=0.7\textwidth]{project1_plot_n=100.pdf}
  \caption{Comparing solutions for $n = 100$ grid points.\label{fig:100}}
\end{figure}
\begin{figure}[h]
  \centering
    \includegraphics[width=0.7\textwidth]{project1_plot_n=1000.pdf}
  \caption{Comparing solutions for $n = 1000$ grid points.\label{fig:1000}}
\end{figure}

\subsection*{(c)}
We have computed the relative error as 
\[
   \epsilon_i=log_{10}\left(\left|\frac{v_i-u_i}
                 {u_i}\right|\right)
\]
as a function of $log_{10}(h)$ for different values of n. The maximum values of the error for different n are shown in table \ref{tab:1}. We see that smaller $h$ gives more accurate results. 
\begin{table}[h]
  \centering
  \begin{tabular}{|c|c|c|}
    \hline
    n & Max error & log10(h) \\ \hline
    10 & -1.1797 & -1.04139 \\ \hline
    100 & -3.08804 & -2.00432 \\ \hline
    1000 & -5.08005 & -3.00043 \\ \hline
    10000 & -7.07936 & -4.00004 \\ \hline
    $10^5$ & -9.0049 & -5 \\ \hline
  \end{tabular}
  \caption{Maximum value of the error as a function of $log10(h)$.\label{tab:1}}
\end{table}


\subsection*{(d)}
Finally we solved the linear equations using LU decomposition. We measured the time it took to run the LU decomposition and the tridiagonal solver to compare the time usage for the different methods. The result is shown in table \ref{tab:2}. We see that the tridiagonal solver is a lot faster than the LU-decomposition.

It takes $O(n^2)$ floating point operations to solve the set of linear equations with an LU decomposed matrix, but it requires $O(n^3)$ floating point operations to obtain the LU decomposed matrix. We can not run the LU decomposition for a matrix of the size $10^5 \times 10^5$.
When trying to run the program for $n=10^5$ we get an error saying the requested size is to large.
\begin{table}
  \centering
  \begin{tabular}{|c|c|c|}
    \hline
    n & LU decomposition [s] & tridiagonal solver [s] \\ \hline
    10 & 0.001224 & 3e-06\\ \hline
    100 & 0.00138 & 8e-06\\ \hline
    1000 & 0.099407 & 9.1e-05\\ \hline
  \end{tabular}
  \caption{Time usage for LU-decomposition and tridiagonal solver for different values of n.\label{tab:2}}
\end{table}


\end{document}
