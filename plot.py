from numpy import *
import matplotlib.pyplot as plt

v = []
u = []
x = []
f = open('values for plotting.txt', 'r')
for line in f:
	values = line.split()
	v.append(values[0])
	u.append(values[1])
	x.append(values[2])

n = len(v)
v = array(v)
u = array(u)
x = array(x)

plt.plot(x,u, label='Closed-form solution u(x)')
plt.legend()
plt.title('n = %g' %(n-2))
plt.xlabel('x')
plt.ylabel('u')
plt.hold('on')
plt.plot(x,v, label='Numerical solution v(x)')
plt.legend()
plt.savefig('project1 plot n=%g.pdf' %(n-2), bbox_inches='tight')
plt.show()

